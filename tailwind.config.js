module.exports = {
  content: [],
  purge: {
    enabled: true,
    content: ['./dist/**/*.html'],
    },
  darkMode: 'class',
  theme: {
    debugScreens: {
      position: ['top', 'left'],
    },
    screens: {
      'xsm' : '375px',
      'xsl' : '425px',
      'sm' : '640px',
      'md' : '768px',
      'lg' : '1024px',
      'xl' : '1440px',
    },
    extend: {
      fontFamily: {
        alata: "'Alata', sans-serif",
        josefin: "'Josefin Sans', sans-serif",
      },
      spacing: {
        '41': '165px',
        '25': '100px',
        '15.5': '60px',
        '7.5': '30px',
        //Naknadno dodato
        '33': '130px',
        '45': '185px',
        '30' : '120px',
        '11.5': '47px',
        '38' : '148px',
        '15': '58px',
        '11.25' : '45px',
        '17': '70px',
        '27':'110px',
        '97':'450px',
        '13':'50px',
        '18': '73px',
        '37': '145px',
        //provjeriti l za 45px i promjeniti sa 11.25
        '19': '76px',
        '25': '100px'
      },
      letterSpacing: {
        adv: '.20em',
        advanced: '.30em',
      },
      lineHeight: {
        'extra-loose': '2.5',
        '12': '3rem',
      },
      maxWidth: {
        'imBlock': '660px',
        'imgBlock': '1200px',
        'bBlock': '180px'
      },
      colors: {
        darkGray: '#8c8c8c',
        veryDarkGray: '#696969',
      },
      fontSize: {
        base: ['15px'],
      },
      width: {
        'border-width': '40.625rem',
        'border-box-width': '35rem',
        'border-width-xl': '30rem',
        'border-width-lg': '22rem',
        'border-width-sm': '18rem',
        'border-width-xsm': '14rem',
      },
    },
  },
  plugins: [
    require('tailwindcss-debug-screens'),
  ],
}
